<?php

namespace Drupal\bitcoin_broker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Returns responses for bitcoin_broker routes.
 */
class BitcoinBrokerController extends ControllerBase {
  /**
   * return price
   */
  public function returnPrice(Request $request) {
    return new JsonResponse($this->getPrice());
  }
  public function getPrice(){
    return rand(40000.00, 60000.00);
  }
}
